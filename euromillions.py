from urllib.request import urlopen
from tkinter import Tk,Listbox,LabelFrame,Entry

def create_list():
    tirage=[]
    tirage_gold=[]
    for line in urlopen('http://lottery.merseyworld.com/cgi-bin/lottery?days=20&Machine=Z&Ballset=0&order=1&show=1&year=0&display=CSV'):
        strline=str(line)
        ligne=strline.split(',')
        if len(ligne)==14 :
            if ligne[2]<'31':
                tirage.extend(ligne[5:10])
                tirage_gold.extend(ligne[10:12])
    return(tirage,tirage_gold)
def fill_freq(tirage,listbox):
    freq_tirage=dict()
    result=[]
    for i in tirage:
        j=i.strip()
        if  j in freq_tirage.keys():
            freq_tirage[j]+=1
        else:
            freq_tirage[j]=1
    for k, v in freq_tirage.items():
        result.append([v,k])
    result.sort(reverse=True)
    for i in result:
        listbox.insert('end',"{} TIMES:{}".format(i[0],i[1]))
def table_freq():
    tirage=[]
    tirage_gold=[]
    freq_tirage=dict()
    result=[]
    result2=[]
    (tirage,tirage_gold)=create_list()
    fill_freq(tirage,lb)
    fill_freq(tirage_gold,lbg)


root=Tk()
frame_freq=LabelFrame(root,text="frequence")
frame_freqg=LabelFrame(root,text="frequence gold")
lb=Listbox(frame_freq)
lbg=Listbox(frame_freqg)
frame_freq.grid(column=1,row=0)
frame_freqg.grid(column=2,row=0)
lb.grid(column=0,row=0)
lbg.grid(column=0,row=0)
table_freq()
root.mainloop()
